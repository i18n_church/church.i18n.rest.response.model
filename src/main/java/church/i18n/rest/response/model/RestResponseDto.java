/*
 * Copyright (c) 2019 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.rest.response.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Simple Rest response DTO.
 *
 * @param <D> Type of response data this class holds.
 */
public final class RestResponseDto<D extends Serializable> implements RestResponse<D> {

  private static final long serialVersionUID = -8629452386411343865L;
  private final D data;
  private final List<ResponseMessage> messages;

  /**
   * Constructor of rest response object.
   *
   * @param data     Response data.
   * @param messages Additional list of messages returned with or without data.
   */
  public RestResponseDto(final D data, final List<ResponseMessage> messages) {
    this.data = data;
    this.messages = messages;
  }

  @Override
  public D getData() {
    return data;
  }

  @Override
  public List<ResponseMessage> getMessages() {
    return messages;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof RestResponseDto)) {
      return false;
    }
    RestResponseDto<?> that = (RestResponseDto<?>) o;
    return Objects.equals(data, that.data)
        && Objects.equals(messages, that.messages);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, messages);
  }

  @Override
  public String toString() {
    return "RestResponseDto{"
        + "data=" + data
        + ", messages=" + messages
        + '}';
  }
}
