/*
 * Copyright (c) 2019 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.rest.response.model;

import java.util.List;
import java.util.Objects;

/**
 * Default Rest Error Response in the case the application encounters exception or there is other
 * type of the message that should be returned to the user together with data. This response is
 * holding base information and no more for proper error response:
 * <ol>
 * <li>
 *   <b>code</b>: Code for automatic processing of errors returned by the application.
 * </li>
 * <li>
 *   <b>message</b>: Human readable and understandable message. The message should contain enough
 *   information to understand what was the problem. However it should not include any sensitive
 *   information that can be used by attacker to abuse them. This includes such as passwords,
 *   internal application structure, libraries, versions...
 *   <b>Please avoid them, this is your responsibility.</b>
 * </li>
 * <li>
 *   <b>severity</b>: Is a severity of the returned message.
 * </li>
 * <li>
 *   <b>helpUri</b>: Is an URI into KB or documentation where user can find more about this error.
 * </li>
 * <li>
 *   <b>context</b>: Contains list of variables, usually that are included in the message or are in
 *   other way important for automatic error handling so the consumer of the message is not required
 *   to parse message and have enough information to process the exception. For the structure
 *   {@link RestResponseDto}
 * </li>
 * </ol>
 */
public final class ResponseMessageDto implements ResponseMessage {

  private static final long serialVersionUID = 5756648629963090479L;
  private final String code;
  private final String message;
  private final String severity;
  private final String helpUri;
  private final List<ResponseContextInfo> context;

  /**
   * Base constructor of response message object.
   *
   * @param code     Code for automatic processing of errors returned by the application.
   * @param message  Human readable and understandable message. The message should contain enough
   *                 information * to understand what was the problem. However it should not include
   *                 any sensitive information that can be used by * attacker to abuse them. This
   *                 includes such as passwords, internal application structure, libraries,
   *                 versions... <b>Please avoid them, this is your responsibility.</b>
   * @param severity Is a severity of the returned message.
   * @param helpUri  Is an URI into KB or documentation where user can find more about this error.
   * @param context  Contains list of variables, usually that are included in the message or are in
   *                 other way * important for automatic error handling so the consumer of the
   *                 message is not required to parse message and have * enough information to
   *                 process the exception. For the structure {@link RestResponseDto}
   */
  public ResponseMessageDto(final String code, final String message, final String severity,
      final String helpUri,
      final List<ResponseContextInfo> context) {
    this.code = code;
    this.message = message;
    this.severity = severity;
    this.helpUri = helpUri;
    this.context = context;
  }

  @Override
  public String getCode() {
    return code;
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public String getHelpUri() {
    return helpUri;
  }

  @Override
  public List<ResponseContextInfo> getContext() {
    return context;
  }

  @Override
  public String getSeverity() {
    return severity;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof ResponseMessageDto)) {
      return false;
    }
    ResponseMessageDto that = (ResponseMessageDto) o;
    return Objects.equals(code, that.code)
        && Objects.equals(message, that.message)
        && Objects.equals(severity, that.severity)
        && Objects.equals(helpUri, that.helpUri)
        && Objects.equals(context, that.context);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, message, severity, helpUri, context);
  }

  @Override
  public String toString() {
    return "ResponseMessageDto{"
        + "code='" + code + '\''
        + ", message='" + message + '\''
        + ", severity=" + severity
        + ", helpUri='" + helpUri + '\''
        + ", context=" + context
        + '}';
  }
}
