/*
 * Copyright (c) 2019 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.rest.response.model;

import java.io.Serializable;
import java.util.List;

/**
 * Base Rest response definition that holds both - data and response messages.
 *
 * @param <D> Type of returned data by the endpoint. Those is your typical answer by the endpoint.
 *            Those data should be serializable to support transfer across systems.
 */
public interface RestResponse<D extends Serializable> extends Serializable {

  /**
   * Returned data by the endpoint.
   *
   * @return Data response returned to the user.
   */
  D getData();

  /**
   * List of messages, either error messages, either informational or warning messages. They can be
   * returned together with data or alone as the error response.
   *
   * @return List of messages that should be returned to the user.
   */
  List<ResponseMessage> getMessages();
}
