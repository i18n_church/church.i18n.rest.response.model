/*
 * Copyright (c) 2019 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.rest.response.model;

import java.util.Objects;

/**
 * Contextual Information class that can be presented to the user.
 */
public final class ContextInfoDto implements ResponseContextInfo {

  private static final long serialVersionUID = -6398338285535454399L;
  private final String name;
  private final String type;
  private final String value;
  private final String format;
  private final String formatType;
  private final String message;

  /**
   * Contextual information data holder.
   *
   * @param name       Name of the parameter, property or validated field. E.g. email, name
   * @param type       Type of the value. For example: BOOLEAN, STRING, OBJECT...
   * @param value      The original value that was provided by the user, computed value by the
   *                   system or other value system input value.
   * @param format     Specifies the correct or expected format of the data. When it is not met, you
   *                   may instruct consumer of your API by specifying the format of the input
   *                   parameter your application expects. E.g. for simple e-mail format it may hold
   *                   regular expression: {@code ^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$}.
   * @param formatType The type of the format argument you have provided. For example when the
   *                   format of data type is an object you can closely specify what type of object
   *                   is expected - e.g. JSON, XML. Or similarly, when you specify the parameter is
   *                   of a type string, you may specify that string is of a array or enum format.
   * @param message    Message providing more detailed human-readable information about the
   *                   parameter.
   */
  public ContextInfoDto(final String name, final String type, final String value,
      final String format, final String formatType, final String message) {
    this.name = name;
    this.type = type;
    this.value = value;
    this.format = format;
    this.formatType = formatType;
    this.message = message;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public String getValue() {
    return value;
  }

  @Override
  public String getFormat() {
    return format;
  }

  @Override
  public String getFormatType() {
    return formatType;
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof ContextInfoDto)) {
      return false;
    }
    ContextInfoDto that = (ContextInfoDto) o;
    return Objects.equals(name, that.name)
        && Objects.equals(type, that.type)
        && Objects.equals(value, that.value)
        && Objects.equals(format, that.format)
        && Objects.equals(formatType, that.formatType)
        && Objects.equals(message, that.message);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, type, value, format, formatType, message);
  }

  @Override
  public String toString() {
    return "ContextInfoDto{"
        + "name='" + name + '\''
        + ", type='" + type + '\''
        + ", value='" + value + '\''
        + ", format='" + format + '\''
        + ", formatType='" + formatType + '\''
        + ", message='" + message + '\''
        + '}';
  }
}
