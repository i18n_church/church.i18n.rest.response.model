# Response model
> Default REST response format holding all important information

Default implementation of a standard response format with the following structure:
```json5
{
  "data": "<null, array or any jackson object>",
  "messages": [{
    "code": "err-r-15",
    "severity": "ERROR", 
    "message": "Http request method 'GET' not allowed at this endpoint. Supported methods are: [POST, DELETE].", 
    "helpUri": "https://dev.i18n.church/error-codes", 
    "context": [{
      "name": "method",
      "type": "STRING",
      "value": "GET",
      "format": "[POST, DELETE]",
      "formatType": "ENUM",
      "message": "Use one of listed methods instead.",
    }] 
  }]
}
```

### Model description
* **data**    Response data.
* **messages** Additional list of returned messages with or without data.
    * **code**     Code for automatic processing of errors returned by the application.
    * **message**  Human readable and understandable message. The message should contain enough
  information to understand what has caused problem. However it should not include
  any sensitive information that can be used by an attacker to abuse them. This
  includes such as passwords, internal application structure, libraries,
  versions... *Please avoid them, this is your responsibility.*
    * **severity** Is a severity of the returned message.
    * **helpUri**  Is an URI of KB or documentation where user can find more about this error.
    * **context**  Contains list of variables, usually those are included in the message alone. Or 
     it holds other important information that are useful for automatic error handling so the 
     consumer of the message is not required to parse message and have enough information to
  process the exception.
        * **name** Name of the parameter, property or validated field. E.g. email, name
        * **type** Type of the value. For examples see: {@link I18nDataType}.
        * **value** The original value that was provided by the user, computed value by the
   system or other value system input value.
        * **format** Specifies the correct or expected format of the data. When it is not met, you
   may instruct consumer of your API what format of the data is expected. E.g. for simple e-mail 
   format it may hold regular expression: ^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$.
        * **formatType** The type of the format argument you have provided. For example when the 
        format of data type is an object you can closely specify what type of object is expected - 
        e.g. JSON, XML. Or similarly, when you specify the parameter is of a type string, you may 
        specify that string is of a array or enum format.
        * **message** Message providing more detailed human-readable information about the 
        parameter.
